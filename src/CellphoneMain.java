public class CellphoneMain {
    public static void main(String[] args){
        Cellphone cp = new Cellphone("Nokia", "3310");
        cp.powerOff();
        // function tidak bisa digunakan karena HP mati 
        cp.volumeDown();
        cp.sisaPulsa();

        cp.powerOn();
        // function bisa digunakan karena HP hidup
        cp.powerOn();
        cp.volumeUp();
        cp.getVolume();
        cp.setVol(30);
        cp.getVolume();

        // Top Up pulsa diatas minimal 5000 & HP posisi hidup,
        // ketika posisi mati pulsa terkirim namun tidak bisa terlihat
        cp.topUpPulsa(20000);
        cp.sisaPulsa();

        // Menambahkan banyak contact dengan Array List
        Contact contact = new Contact("Evan", "152463");
        contact.tambahContact("Aziz", "136524");
        contact.tambahContact("Adji", "254631");
        contact.tambahContact("Daffa", "652145");
        contact.tambahContact("Rama", "854123");
        contact.displayContacts();

        // Mencari Contact dengan Nama dan No
        contact.showContactByName("Daffa");
        contact.showContactByNoHP("652145");

    }
}
